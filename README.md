# GeoLib

Kotlin library to showcase some basic shapes with translate operation.

**Importing project**

This project can be opened in InelliJ IDEA for Java. Use root level build.gradle to open the project.

**Running tests**

The tests can be run using IntelliJ IDEA.

    1. Right click on test folder
    2. Click -> Run "Tests in 'GeoLibrary.."
    3. For coverage click -> Run "Tests in 'GeoLibrary.." with Coverage

The tests also can be run using command line 

    On Windows
        - Open command prompt and move to project root directory.
        - Run "gradlew.bat test"
        
    On Mac or Linux
        - Open terminal and move to project root directory.
        - Run "./gradlew test"