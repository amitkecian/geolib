package com.example.geospatial.geometry.distance

import com.example.geospatial.geometry.shape.Point
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

class EuclideanPointsDistanceCalculatorTest {

    @Test
    fun `verify distance between Ams Central and Utrecht Central is between 350000 to 360000 meters`() {
        val amsPoint = Point(52.3791, 4.9003, 0.0)
        val utrechtPoint = Point(52.0894, 5.1100, 0.0)

        val euclideanPointsDistanceCalculator =  EuclideanPointsDistanceCalculator()
        val distance = euclideanPointsDistanceCalculator.calculateDistanceWithPoints(amsPoint, utrechtPoint)

        assertTrue { distance > 35000 }
        assertTrue { distance < 36000 }
    }

    @Test
    fun `verify distance is zero between same points`() {
        val amsPoint = Point(52.3791, 4.9003, 0.0)

        val euclideanPointsDistanceCalculator =  EuclideanPointsDistanceCalculator()
        val distance = euclideanPointsDistanceCalculator.calculateDistanceWithPoints(amsPoint, amsPoint)

        assertEquals(0.0, distance)
    }
}