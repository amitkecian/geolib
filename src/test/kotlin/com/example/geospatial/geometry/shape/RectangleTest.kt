package com.example.geospatial.geometry.shape

import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock

class RectangleTest {
    private val startPoint = Point(52.3791, 4.9003, 0.0) //Amsterdam Centraal
    private val endPoint = Point(52.0894, 5.1100, 0.0) //Utrecht Centraal

    @Test
    fun `verify test point is out of Rectangle`() {
        //Paris doesn't intersect between ams and utrecht
        val testPoint = Point(48.8566, 2.3522, 0.0)
        val rectangle = Rectangle(startPoint, endPoint)
        assertFalse { rectangle.containsPoint(testPoint) }
    }

    @Test
    fun `verify test point is in range of Rectangle`() {
        //Amsterdam-Zuidoost lies between Utrecht and Amsterdam
        val testPoint = Point(52.3080, 4.9715, 0.0)
        val rectangle = Rectangle(startPoint, endPoint)
        assertTrue { rectangle.containsPoint(testPoint) }
    }

    @Test
    fun `verify test point is in range of Rectangle if start and end points are swapped`() {
        //Amsterdam-Zuidoost lies between Utrecht and Amsterdam
        val testPoint = Point(52.3080, 4.9715, 0.0)
        val rectangle = Rectangle(endPoint, startPoint)
        assertTrue { rectangle.containsPoint(testPoint) }
    }

    @Test
    fun `verify translated Rectangle has different reference than original rectangle`() {
        val startPoint = mock(Point::class.java)
        val endPoint = mock(Point::class.java)

        val vector = mock(Vector::class.java)

        `when`(startPoint.translate(vector)).thenReturn(startPoint)
        `when`(endPoint.translate(vector)).thenReturn(endPoint)

        val rectangle = Rectangle(startPoint, endPoint)

        assertFalse(rectangle === rectangle.translate(vector))
    }
}