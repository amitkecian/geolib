package com.example.geospatial.geometry.shape

import com.example.geospatial.geometry.distance.PointsDistanceCalculator
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.mockito.Mockito
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import java.security.InvalidParameterException

class LineTest {

    private lateinit var startPoint: Point
    private lateinit var endPoint: Point

    @BeforeEach
    fun setUp() {
        startPoint = Point(30.0, 180.0, 0.0)
        endPoint = Point(50.0, 180.0, 0.0)
    }

    @Test
    fun `verify Line throws error if start and end points are same`() {
        assertThrows<InvalidParameterException> {
            Line(startPoint, startPoint)
        }
    }

    @Test
    fun `verify correct line length is returned`() {
        val line = Line(startPoint, endPoint)
        val mockDistanceCalculator = mock(PointsDistanceCalculator::class.java)
        `when`(mockDistanceCalculator.calculateDistanceWithPoints(startPoint, endPoint))
            .thenReturn(53000.0)
        assertEquals(53000.0, line.length(mockDistanceCalculator))
    }

    @Test
    fun `verify translated Line has different reference than original line`() {
        val startPoint = mock(Point::class.java)
        val endPoint = mock(Point::class.java)

        val vector = mock(Vector::class.java)

        val translatedStartPoint = mock(Point::class.java)
        val translatedEndPoint = mock(Point::class.java)

        `when`(startPoint.translate(vector)).thenReturn(translatedStartPoint)
        `when`(endPoint.translate(vector)).thenReturn(translatedEndPoint)

        val line = Line(startPoint, endPoint)

        val translatedLine = line.translate(vector)

        Mockito.verify(startPoint).translate(vector)
        Mockito.verify(endPoint).translate(vector)

        assertNotEquals(line.startPoint, translatedLine.startPoint)
        assertNotEquals(line.endPoint, translatedLine.endPoint)

        assertEquals(translatedStartPoint, translatedLine.startPoint)
        assertEquals(translatedEndPoint, translatedLine.endPoint)

        assertFalse(line === translatedLine)
    }
}