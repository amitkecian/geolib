package com.example.geospatial.geometry.shape

import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.mockito.Mockito
import org.mockito.Mockito.*
import java.security.InvalidParameterException

class TwoPointShapeTest {
    private val startPoint = mock(Point::class.java)
    private val endPoint = mock(Point::class.java)
    private val vector = mock(Vector::class.java)

    private var shape = mock(
        TwoPointShape::class.java,
        withSettings().useConstructor(startPoint, endPoint).defaultAnswer(CALLS_REAL_METHODS)
    )

    @Test
    fun `verify TwoPointShape throws error if start and end points are same`() {
        assertThrows<InvalidParameterException> {
            Line(startPoint, startPoint)
        }
    }

    @Test
    fun `verify TwoPointShape translation is not possible if start point translation is not possible`() {
        `when`(startPoint.canTranslateShape(vector)).thenReturn(false)
        `when`(endPoint.canTranslateShape(vector)).thenReturn(false)

        assertFalse(shape.canTranslateShape(vector))
    }

    @Test
    fun `verify TwoPointShape translation is not possible if end point translation is not possible`() {
        `when`(startPoint.canTranslateShape(vector)).thenReturn(false)
        `when`(endPoint.canTranslateShape(vector)).thenReturn(false)

        assertFalse(shape.canTranslateShape(vector))
    }

    @Test
    fun `verify TwoPointShape translation possible if both start and end point can be translated`() {
        `when`(startPoint.canTranslateShape(vector)).thenReturn(true)
        `when`(endPoint.canTranslateShape(vector)).thenReturn(true)

        assertTrue(shape.canTranslateShape(vector))
    }
}