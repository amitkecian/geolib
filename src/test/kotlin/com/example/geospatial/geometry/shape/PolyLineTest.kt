package com.example.geospatial.geometry.shape

import com.example.geospatial.geometry.distance.PointsDistanceCalculator
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.mockito.Mockito.*
import java.security.InvalidParameterException

class PolyLineTest {

    @Test
    fun `verify Polyline throws error if has zero or one point`() {
        assertThrows<InvalidParameterException> {
            PolyLine(listOf())
        }

        assertThrows<InvalidParameterException> {
            PolyLine(listOf(mock(Point::class.java)))
        }
    }

    @Test
    fun `verify Polyline throws error if it contains two same consecutive points`() {
        val pointA = mock(Point::class.java)
        val pointB = mock(Point::class.java)

        assertThrows<InvalidParameterException> {
            PolyLine(listOf(pointA, pointB, pointB))
        }
    }

    @Test
    fun `verify Polyline can be represented in list of Lines`() {
        val pointA = mock(Point::class.java)
        val pointB = mock(Point::class.java)
        val pointC = mock(Point::class.java)
        val pointD = mock(Point::class.java)

        val polyLine = PolyLine(listOf(pointA, pointB, pointC, pointD))
        val lines = polyLine.getLines()

        assertEquals(3, lines.size)

        //Assert Line1 contains pointA and pointB as start/end points
        assertEquals(pointA, lines[0].startPoint)
        assertEquals(pointB, lines[0].endPoint)

        //Assert Line2 contains pointB and pointC as start/end points
        assertEquals(pointB, lines[1].startPoint)
        assertEquals(pointC, lines[1].endPoint)

        //Assert Line3 contains pointC and pointD as start/end points
        assertEquals(pointC, lines[2].startPoint)
        assertEquals(pointD, lines[2].endPoint)
    }

    @Test
    fun `verify translation fails if any point can't be translated`() {
        val pointA = mock(Point::class.java)
        val pointB = mock(Point::class.java)
        val vector = mock(Vector::class.java)

        `when`(pointA.canTranslateShape(vector)).thenReturn(true)
        `when`(pointB.canTranslateShape(vector)).thenReturn(false)

        val polyLine = PolyLine(listOf(pointA, pointB))
        assertFalse(polyLine.canTranslateShape(vector))
    }

    @Test
    fun `verify Polyline can be translated if all points can be translated`() {
        val pointA = mock(Point::class.java)
        val pointB = mock(Point::class.java)
        val vector = mock(Vector::class.java)

        `when`(pointA.canTranslateShape(vector)).thenReturn(true)
        `when`(pointB.canTranslateShape(vector)).thenReturn(true)

        val polyLine = PolyLine(listOf(pointA, pointB))
        assertTrue(polyLine.canTranslateShape(vector))
    }

    @Test
    fun `verify Polyline translate produces different Polyline object with different points`() {
        val pointA = mock(Point::class.java)
        val pointB = mock(Point::class.java)
        val vector = mock(Vector::class.java)

        val polyLine = PolyLine(listOf(pointA, pointB))

        `when`(pointA.translate(vector)).thenReturn(mock(Point::class.java))
        `when`(pointB.translate(vector)).thenReturn(mock(Point::class.java))

        val translatedPolyLine = polyLine.translate(vector)

        verify(pointA).translate(vector)
        verify(pointB).translate(vector)

        assertNotEquals(polyLine.points, translatedPolyLine.points)

        assertNotEquals(polyLine, translatedPolyLine)
    }

    @Test
    fun `verify Polyline return valid point if the position is in Range`() {
        val mockDistanceCalculator = mock(PointsDistanceCalculator::class.java)
        val polyLine = getPolyline(mockDistanceCalculator)

        assertNotNull(polyLine.getPointAtPosition(6000.0, mockDistanceCalculator))
    }

    @Test
    fun `verify Polyline returns first point if the requested position is 0 meters`() {
        val mockDistanceCalculator = mock(PointsDistanceCalculator::class.java)
        val startPoint = Point(52.3791, 4.9003, 0.0)

        val polyLine = getPolyline(mockDistanceCalculator)

        val pointAtPosition = polyLine.getPointAtPosition(0.0, mockDistanceCalculator)

        assertEquals(startPoint, pointAtPosition)
    }

    @Test
    fun `verify Polyline returns last end point if the requested position is equals total distance`() {
        val mockDistanceCalculator = mock(PointsDistanceCalculator::class.java)
        val endPoint = Point(52.0894, 5.1100, 0.0)
        val polyLine = getPolyline(mockDistanceCalculator)

        val pointAtPosition = polyLine.getPointAtPosition(50600.0, mockDistanceCalculator)

        assertEquals(endPoint, pointAtPosition)
    }

    @Test
    fun `verify Polyline return null point if the requested position is out total distance of Polyline`() {
        val mockDistanceCalculator = mock(PointsDistanceCalculator::class.java)
        val polyLine = getPolyline(mockDistanceCalculator)

        assertNull(polyLine.getPointAtPosition(106000.0, mockDistanceCalculator))
    }

    private fun getPolyline(mockDistanceCalculator: PointsDistanceCalculator): PolyLine {
        val pointA = Point(52.3791, 4.9003, 0.0)
        val pointB = Point(52.3080, 4.9715, 0.0)
        val pointC = Point(52.0894, 5.1100, 0.0)

        //Assume distance between A and B 15600 meters
        `when`(mockDistanceCalculator.calculateDistanceWithPoints(pointA, pointB))
            .thenReturn(15600.0)

        //Assume distance between B and C 35000 meters
        `when`(mockDistanceCalculator.calculateDistanceWithPoints(pointB, pointC))
            .thenReturn(35000.0)

        //Total distance of the Polyline is 50600
        return PolyLine(listOf(pointA, pointB, pointC))
    }
}