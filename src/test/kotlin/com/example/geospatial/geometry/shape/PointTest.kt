package com.example.geospatial.geometry.shape

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.security.InvalidParameterException

class PointTest {
    @Test
    fun `verify Latitude greater than 90 throws error`() {
        assertThrows<InvalidParameterException> {
            Point(120.0, 180.0, 0.0)
        }
    }

    @Test
    fun `verify Latitude less than -90 throws error`() {
        assertThrows<InvalidParameterException> {
            Point(-120.0, 180.0, 0.0)
        }
    }

    @Test
    fun `verify Point can be translated if latitude sum in range of -90 to 90 degrees`() {
        val point = Point(30.0, 180.0, 0.0)
        val vector = Vector(40.0, 10.0, 0.0)

        assertTrue(point.canTranslateShape(vector))
    }

    @Test
    fun `verify Point can be translated if latitude sum is 90 degrees`() {
        val point = Point(30.0, 180.0, 0.0)
        val vector = Vector(60.0, 10.0, 0.0)

        assertTrue(point.canTranslateShape(vector))
    }

    @Test
    fun `verify Point can be translated if latitude sum is -90 degrees`() {
        val point = Point(30.0, 180.0, 0.0)
        val vector = Vector(-120.0, 10.0, 0.0)

        assertTrue(point.canTranslateShape(vector))
    }

    @Test
    fun `verify Point can not be translated if latitude sum crosses 90 degrees`() {
        val point = Point(30.0, 180.0, 0.0)
        val vector = Vector(70.0, 10.0, 0.0)

        assertFalse(point.canTranslateShape(vector))
    }

    @Test
    fun `verify Point can not be translated if latitude sum is below -90 degrees`() {
        val point = Point(30.0, 180.0, 0.0)
        val vector = Vector(-150.0, 10.0, 0.0)

        assertFalse(point.canTranslateShape(vector))
    }

    @Test
    fun `verify new translated Point contains different reference than original Point with correct values`() {
        val point = Point(30.0, 120.0, 10.0)
        val vector = Vector(40.0, 180.0, 20.0)

        val newPoint = point.translate(vector)

        assertEquals(70.0, newPoint.latitude)
        assertEquals(-60.0, newPoint.longitude)
        assertEquals(30.0, newPoint.elevation)

        assertFalse(newPoint === point)
    }

    @Test
    fun `verify Longitude for Point is normalized if less than -180`() {
        val point = Point(30.0, -200.0, 10.0)

        assertEquals(160.0, point.longitude)
    }

    @Test
    fun `verify Longitude for Point is normalized if greater than 180`() {
        val point = Point(30.0, 300.0, 10.0)

        assertEquals(-60.0, point.longitude)
    }
}