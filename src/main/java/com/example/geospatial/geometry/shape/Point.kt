package com.example.geospatial.geometry.shape

import com.example.geospatial.geometry.GeometricalConstants.LATITUDE_MAX
import com.example.geospatial.geometry.GeometricalConstants.LATITUDE_MIN
import com.example.geospatial.geometry.GeometricalConstants.LONGITUDE_MAX
import com.example.geospatial.geometry.GeometricalConstants.LONGITUDE_MIN
import java.security.InvalidParameterException

/**
 * Represents a geographic location.
 *
 * @param latitude Latitude of the location. Valid values are in the range of [LATITUDE_MIN] and [LATITUDE_MAX].
 *
 * @param lng Longitude of the location.
 *
 * @param elevation Distance from WGS84 ellipsoid.
 *
 * @throws IllegalAccessException if [latitude] is in range of [LATITUDE_MIN] and [LATITUDE_MAX].
 */
class Point(val latitude: Double, lng: Double, val elevation: Double) : Shape<Point> {
    init {
        validateLatitude(latitude)
    }

    /**
     * Provides the normalized longitude between range of [LONGITUDE_MIN] and [LONGITUDE_MAX].
     */
    val longitude = normalizeLongitude(lng)

    private fun validateLatitude(lat: Double) {
        if (lat > LATITUDE_MAX || lat < LATITUDE_MIN) {
            throw InvalidParameterException("$lat out of range. Possible values are between $LATITUDE_MIN and $LATITUDE_MAX")
        }
    }

    /**
     * @see [https://github.com/elastic/elasticsearch/blob/420f7a732b3882c297bc7901060d3641adb794c7/server/src/main/java/org/elasticsearch/common/geo/GeoUtils.java#L341]
     */
    private fun normalizeLongitude(lng: Double): Double {
        val longitudeOutOfRange = lng > LONGITUDE_MAX || lng <= LONGITUDE_MIN
        if (!longitudeOutOfRange) {
            return lng
        }

        val divisor = 360
        var result = lng % divisor

        if (result <= 0) {
            result += divisor
        }

        if (result > divisor / 2) {
            result -= divisor
        }
        return result
    }

    /**
     * Provides the info if the translation is possible.
     * Translation will fail if the new calculated latitude is in not in range of -90 to 90 degrees.
     *
     * @param vector that provides information about how much [Point] should be translated.
     */
    override fun canTranslateShape(vector: Vector): Boolean {
        val newLat = latitude + vector.northShiftInDegrees
        return (-90.0..90.0).contains(newLat)
    }

    /**
     * Returns a new [Point] after translation.
     *
     * @see [Point.canTranslateShape] before using this method.
     *
     * @param vector that provides information about how much [Point] should be translated.
     */
    override fun translate(vector: Vector): Point {
        return Point(latitude + vector.northShiftInDegrees,
            normalizeLongitude(longitude + vector.eastShiftInDegree),
            elevation + vector.elevationChangeInMeters
        )
    }

    override fun hashCode(): Int {
        return latitude.hashCode() + longitude.hashCode() + elevation.hashCode()
    }

    override fun equals(other: Any?): Boolean {
        if(other !is Point) {
            return false
        }
        return other.latitude == latitude
                && other.longitude == longitude
                && other.elevation == elevation
    }
}