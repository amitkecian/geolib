package com.example.geospatial.geometry.shape

import com.example.geospatial.geometry.distance.PointsDistanceCalculator
import java.security.InvalidParameterException


/**
 * Class representing a [PolyLine] constructed with a ordered list of [Point].
 *
 * @param points used to create a [PolyLine] using the [Point] in order.
 *
 * @throws [InvalidParameterException] if the [points] size is less than 2
 * and if any two consecutive points are same.
 */
class PolyLine(val points: List<Point>) : Shape<PolyLine> {

    init {
        validateContainsMinimumTwoPoints()
        validContainsDistinctPoints()
    }

    private fun validateContainsMinimumTwoPoints() {
        if (points.size < 2) {
            throw InvalidParameterException("PolyLine must contains 2 or more points")
        }
    }

    private fun validContainsDistinctPoints() {
        for (index in 0..points.size - 2) {
            val start = points[index]
            val end = points[index + 1]
            if (start == end) {
               throw InvalidParameterException("All consecutive points must be unique")
            }
        }
    }

    /**
     * Check if the [PolyLine] can be translated.
     * @param vector that provides information about how much [Rectangle] should be translated.
     */
    override fun canTranslateShape(vector: Vector): Boolean {
        return canPointsBeTranslated(vector, points)
    }

    /**
     * Translates the [PolyLine] with the provided [Vector].
     *
     * Make sure to check [canTranslateShape]  before calling this to avoid any exceptions.
     *
     * @param vector that provides information about how much [PolyLine] should be translated.
     */
    override fun translate(vector: Vector): PolyLine {
        val translatedPoints = points.map { it.translate(vector) }
        return PolyLine(translatedPoints)
    }

    /**
     * Provides the list of [Line] representing [PolyLine].
     */
    fun getLines(): List<Line> {
        val linesList = mutableListOf<Line>()
        for (index in 0..points.size - 2) {
            val start = points[index]
            val end = points[index + 1]
            linesList.add(Line(start, end))
        }
        return linesList
    }

    /**
     * Provides a 3D point on [PolyLine] from the origin, if any point exists and with in the limit of total distance.
     * It returns null in case the provided [positionInMeter] in greater than the total distance of [PolyLine].
     *
     * @param positionInMeter to obtain a new [Point] on the [PolyLine].
     *
     * @param pointsDistanceCalculator calculates the distance two [Point]s.
     *
     * @return [Point] if the [positionInMeter] less than total distance of [PolyLine] otherwise null.
     */
    fun getPointAtPosition(positionInMeter: Double, pointsDistanceCalculator: PointsDistanceCalculator): Point? {
        if(positionInMeter == 0.0) {
            return points[0]
        }

        var distance = positionInMeter
        for (index in 0..points.size - 2) {
            val start = points[index]
            val end = points[index + 1]
            val distanceBetweenPoints =
                pointsDistanceCalculator.calculateDistanceWithPoints(start, end)

            if (distanceBetweenPoints == distance) {
                return end
            }

            if (distance < distanceBetweenPoints) {
                val fractionDistanceCovered = distance / distanceBetweenPoints
                return transformPoints(start, end, fractionDistanceCovered)
            }

            distance -= distanceBetweenPoints
        }
        return null
    }

    private fun transformPoints(start: Point, end: Point, fractionDistance: Double): Point {
        val newLatitude = start.latitude + (end.latitude - start.latitude) * fractionDistance
        val newLongitude = start.longitude + (end.longitude - start.longitude) * fractionDistance
        val newElevation = start.elevation + (end.elevation - start.elevation) * fractionDistance
        return Point(newLatitude, newLongitude, newElevation)
    }
}