package com.example.geospatial.geometry.shape

/***
 * Describes a logical translation unit.
 *
 * @param northShiftInDegrees target shift in north.
 *
 * @param eastShiftInDegree target shift in east.
 *
 * @param elevationChangeInMeters elevation changes in the meters.
 */
class Vector(
    val northShiftInDegrees: Double,
    val eastShiftInDegree: Double,
    val elevationChangeInMeters: Double
)