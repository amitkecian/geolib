package com.example.geospatial.geometry.shape

/**
 * Utility function to check if all the [Point] can be translate with a given [Vector].
 *
 * @param vector that provides information about how much every [Point] should be translated.
 *
 * @return true if all the points in the list can be translated.
 */
fun canPointsBeTranslated(vector: Vector, points: List<Point>): Boolean {
    for (point in points) {
        if (!point.canTranslateShape(vector)) {
            return false
        }
    }
    return points.isNotEmpty()
}