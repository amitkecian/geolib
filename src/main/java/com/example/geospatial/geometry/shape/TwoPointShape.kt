package com.example.geospatial.geometry.shape

import java.security.InvalidParameterException

/**
 * Generic super class for two [Point] based shapes.
 *
 * @param startPoint [Point] that can be treated as start point.
 *
 * @param endPoint [Point] that can be treated as end point.
 *
 * @throws [InvalidParameterException] if the start and end points are same.
 */
abstract class TwoPointShape<T>(val startPoint: Point, val endPoint: Point) : Shape<T> {
    init {
        assertTwoPointsAreDifferent(startPoint, endPoint)
    }

    private fun assertTwoPointsAreDifferent(start: Point, end: Point) {
        if (start == end) {
            throw InvalidParameterException("Start and End point must be different")
        }
    }

    override fun canTranslateShape(vector: Vector): Boolean {
        return canPointsBeTranslated(vector, listOf(startPoint, endPoint))
    }
}