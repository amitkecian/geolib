package com.example.geospatial.geometry.shape

/**
 * A generic interface defining a [Shape].
 * It provides two methods [canTranslateShape] and [translate] to check and change translate the [Shape] respectively.
 */
interface Shape<T> {

    /**
     * @param vector that provides information about how much [Shape] should be translated.
     *
     * @return true if the translation is possible.
     */
    fun canTranslateShape(vector: Vector): Boolean

    /**
     * Translate the underlying shape with prvoided [Vector].
     * Make sure to check [canTranslateShape] before calling this method to avoid any exceptions
     * in case the translation is not possible for any reasons.
     *
     * @param vector that provides information about how much [Shape] should be translated.
     *
     * @return [T] after proper translation.
     */
    fun translate(vector: Vector): T
}