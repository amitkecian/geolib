package com.example.geospatial.geometry.shape

import kotlin.math.max
import kotlin.math.min

/**
 * This class defines a [Rectangle] constructed from two [Point].
 *
 * @param startPoint [Point] that can be treated as start point.
 *
 * @param endPoint [Point] that can be treated as end point.
 */
class Rectangle(startPoint: Point, endPoint: Point) : TwoPointShape<Rectangle>(startPoint, endPoint) {

    /**
     * Translates the [Rectangle] with the provided [Vector].
     *
     * Make sure to check [canTranslateShape] before calling this method to avoid any exceptions
     *
     * @param vector that provides information about how much [Rectangle] should be translated.
     */
    override fun translate(vector: Vector): Rectangle {
        return Rectangle(startPoint.translate(vector), endPoint.translate(vector))
    }

    /**
     * Function to check if a given point lies in the [Rectangle] bounds.
     *
     * @return true if the given point is inside the [Rectangle].
     */
    fun containsPoint(point: Point): Boolean {
        val maxLatitude = max(startPoint.latitude, endPoint.latitude)
        val minLatitude = min(startPoint.latitude, endPoint.latitude)

        val maxLongitude= max(startPoint.longitude, endPoint.longitude)
        val minLongitude= min(startPoint.longitude, endPoint.longitude)

        val targetLatitudeInRange = point.latitude in minLatitude..maxLatitude
        val targetLongitudeInRange = point.longitude in minLongitude..maxLongitude

        return targetLatitudeInRange && targetLongitudeInRange
    }
}