package com.example.geospatial.geometry.shape

import com.example.geospatial.geometry.distance.PointsDistanceCalculator

/**
 * This class defines a [Line] constructed from two [Point].
 *
 * @param startPoint [Point] that can be treated as start point.
 *
 * @param endPoint [Point] that can be treated as end point.
 */
class Line(startPoint: Point, endPoint: Point) : TwoPointShape<Line>(startPoint, endPoint) {

    /**
     * Calculates the distance of the line between [Line] [startPoint] and [endPoint].
     *
     * This is a costly operation and considering caching or storing it and avoid multiple calls.
     *
     * @param pointsDistanceCalculator calculates the distance using [Line] [startPoint] and [endPoint].
     */
    fun length(pointsDistanceCalculator: PointsDistanceCalculator): Double {
        return pointsDistanceCalculator.calculateDistanceWithPoints(startPoint, endPoint)
    }

    /**
     * Translates the [Line] with the provided [Vector].
     *
     * Make sure to check [canTranslateShape] before calling this method to avoid any exceptions
     *
     * @param vector that provides information about how much [Line] should be translated.
     */
    override fun translate(vector: Vector): Line {
        return Line(startPoint.translate(vector), endPoint.translate(vector))
    }
}