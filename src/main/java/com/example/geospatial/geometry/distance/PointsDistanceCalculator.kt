package com.example.geospatial.geometry.distance

import com.example.geospatial.geometry.shape.Point

/**
 * Interface to delegate the calculation of the distance between two [Point].
 */
interface PointsDistanceCalculator {
    /**
     * Calculates distance between two points.
     * The distance metric unit can be different per implementation and should be
     * documented with the implementation.
     *
     * @param start starting [Point]
     *
     * @param end end or another [Point]
     */
    fun calculateDistanceWithPoints(start: Point, end: Point): Double
}