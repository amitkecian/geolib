package com.example.geospatial.geometry.distance

import com.example.geospatial.geometry.GeometricalConstants.EARTH_RADIUS_IN_METERS
import com.example.geospatial.geometry.shape.Point
import kotlin.math.cos
import kotlin.math.pow
import kotlin.math.sqrt

/**
 * Calculates the Euclidean distance between two points using approximation.
 * This is appropriate only for the shorter distances.
 * The distance returned is in meters.
 */
class EuclideanPointsDistanceCalculator : PointsDistanceCalculator {

    /**
     * Calculates the Euclidean distance between two points using approximation.
     *
     * @param start starting [Point]
     *
     * @param end end or another [Point]
     *
     * @return distance between two points in meters.
     */
    override fun calculateDistanceWithPoints(start: Point, end: Point): Double {
        val latLngDistance = calcDistanceBetweenLatLng(start, end)
        val elevationAdjustment = toRadians(end.elevation) - toRadians(start.elevation)
        return sqrt(latLngDistance.pow(2) + elevationAdjustment.pow(2))
    }

    private fun calcDistanceBetweenLatLng(start: Point, end: Point): Double {
        val x = (toRadians(end.longitude) - toRadians(start.longitude)) * cos(toRadians(start.latitude))
        val y = toRadians(end.latitude) - toRadians(start.latitude)
        return EARTH_RADIUS_IN_METERS * sqrt(x.pow(2) + y.pow(2))
    }

    private fun toRadians(angle: Double) = Math.toRadians(angle)
}