package com.example.geospatial.geometry

object GeometricalConstants {
    /**
     * Radius of Earth in meters.
     */
    const val EARTH_RADIUS_IN_METERS = 6378137.0

    /**
     * Maximum latitude possible.
     */
    const val LATITUDE_MAX: Double = 90.0

    /**
     * Minimum latitude possible.
     */

    const val LATITUDE_MIN: Double = -90.0
    /**
     * Minimum longitude possible.
     */

    const val LONGITUDE_MAX: Double = 180.0
    /**
     * Minimum longitude possible.
     */
    const val LONGITUDE_MIN: Double = -180.0
}